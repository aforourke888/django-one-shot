from django.shortcuts import render, get_object_or_404, redirect
from .models import TodoList, TodoItem
from .forms import TodoListForm, TodoItemForm

# Create your views here.


def todo_list(request):
    todo_lists = TodoList.objects.all()
    context = {
        'todo_lists': todo_lists
    }
    return render(request, 'todos/todo_list.html', context)


def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    tasks = TodoItem.objects.filter(list=todo_list)
    context = {
        'todo_list': todo_list,
        'tasks': tasks,
    }
    return render(request, 'todos/todo_list_detail.html', context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=form.instance.id)
    else:
        form = TodoListForm()

    context = {
        "form": form,
    }
    return render(request, "todos/todo_list_create.html", context)


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todo_item = form.save()
            todo_list = todo_item.list
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = TodoItemForm()

    todo_lists = TodoList.objects.all()
    context = {
        "form": form,
        "todo_lists": todo_lists,
    }
    return render(request, "todos/todo_item_create.html", context)


def todo_list_update(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo_list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoListForm(instance=todo_list)

    context = {
        "form": form,
    }
    return render(request, "todos/todo_list_update.html", context)


def todo_item_update(request, id):
    todo_item = get_object_or_404(TodoItem, id=id)
    if request.method == 'POST':
        form = TodoItemForm(request.POST, instance=todo_item)
        if form.is_valid():
            form.save()
            return redirect('todo_list_detail', id=todo_item.list.id)
    else:
        form = TodoItemForm(instance=todo_item)
    context = {'form': form}
    return render(request, "todos/todo_item_update.html", context)


def todo_list_delete(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todo_list.delete()
        return redirect("todo_list")
    context = {
        "todo_list": todo_list
    }
    return render(request, "todos/todo_list_delete.html", context)
